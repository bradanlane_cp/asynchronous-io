""" ***************************************************************************
* File:    async_input_output.py
* Date:    2024.05.24
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

--------------------------------------------------------------------------

Example of using CircuitPython asyncio for actual input and output

This is a simple example which has one cooperative task generating output
(a message at random intervals) and another task waiting for input.

It is meant to serve as a starting point for more complex applications
such as updating a display or reading an accelerometer while waiting for
user input.

This example is based on material from the following work:
https://github.com/s-light/CircuitPython_nonblocking_serialinput

--------------------------------------------------------------------------
"""

import usb_cdc
import random
import asyncio

'''
    each asyncio 'task' needs to yield as often as possible and for as long as possible
    a value of ZERO will yield and then immediately request to get back into queue
'''
INPUT_SLEEP_INTERVAL = 0
OUTPUT_SLEEP_INTERVAL = 5

running = True  # a single flag to tell asynchronous tasks when to stop

async def output_func():
    '''
    this is the output task
        it is a simple demonstration of periodic output
        at a somewhat random interval
        
        a real application could use this framework to update a display
    '''
    global running
    counter = 0
    
    print("start output_func")
    while running:
        wait = int(OUTPUT_SLEEP_INTERVAL * random.random())
        await asyncio.sleep(wait)
        counter += 1
        #print(f"output: {counter}")
        print('.', end='')
    print("finish output_func")



async def input_func():
    '''
    this is the input task
        it is a simple demonstration of waiting for input
        and then processing that input
        
        while the processing step is application specific,
        this input method is common for CircuitPython serial input
    '''
    global running
    serial = usb_cdc.console
    input_buffer = ''
    have_input = False
        
    print("start input_func")
    if not serial.connected:
        print("no serial connection")
        return
    
    serial.timeout = 0.25
    available = serial.in_waiting
    print("prompt> ", end='')
    while running:  # main loop for the input handler
        if available:
            raw = serial.read(available)
            if b'\n' in raw:
                have_input = True
            text = raw.decode("utf-8")
            input_buffer += text
        if have_input:
            # do something with the input, then reset and continue
            input_buffer = input_buffer.replace('\n', '').replace('\r', '')
            print(f"input: '{input_buffer}' = {input_buffer.encode("utf-8").hex()}")

            if input_buffer == "stop":
                running = False
                break

            input_buffer = ''
            print("prompt> ", end='')
            have_input = False
            
        await asyncio.sleep(INPUT_SLEEP_INTERVAL)
        available = serial.in_waiting
    print("finish input_func")
    

async def start_get_put():
    output_task = asyncio.create_task(output_func())
    input_task = asyncio.create_task(input_func())
    await asyncio.gather(output_task, input_task)
    print("done")

asyncio.run(start_get_put())
