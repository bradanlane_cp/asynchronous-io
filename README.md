Building interactive serial terminal applications with CircuitPython required getting inptu and generating output.
If the entire application is through the serial terminal, then the tranditional `input()` and `print()` functions
are often sufficient.

The goal is for the input and output to be intuitive to the user.
Things can get more complicated with some of the action takes place in hardware connected to the microcontroller.

If the code is waiting on input, then the hardware must wait.
If the hardware is slow, then these waits create a _clunky_ experience for the user.

**Scenario:** User input and output is through the USB serial connection with updates to an ePaper display

The ePaper display can not be refreshed too quickly. At the same time, the user may pause at input.
The best experience is that these unrelated pauses do not affect each other.

**Solution:** Use `asyncio`

Using `asyncio` fells like teh obvious solution but it turns out to be a bit more complicated.
The traditional use of `input()` does not work as it will _block_ further code from executing until the user
has pressed ENTER/RETURN at the end of their input.

Adding `usb_cdc` provides fine grain control of the serial input in a _non-blocking_ loop.

**Example:** Periodic random output while capturing serial input

The example is broken into two parts, the `input_func()` and the `output_func()`.
The output portion is exemplar only since more real solution will have very specific needs.
The input portion may be usable in its entirety _(or most nearly so)_.

**LICENSE:** This example _(and its reference)_ are made available under the MIT open source license